﻿#include "GameOverScene.h"
#include "GameScene.h"

USING_NS_CC;

Scene* GameOverScene::createScene()
{
	auto scene = Scene::create();
	auto layer = GameOverScene::create();
	scene->addChild(layer);
	return scene;
}

bool GameOverScene::init()
{
	if (!Layer::init())
	{
		return false;
	}
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();


	auto bg = Sprite::create("ui/shoot_background/background.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	bg->setAnchorPoint(Vec2(0.5, 0.5));
	this->addChild(bg, 0);

	int currentScore = UserDefault::getInstance()->getIntegerForKey("currentscore");
	int topScore = UserDefault::getInstance()->getIntegerForKey("topscore");
	//当前分数
	auto label = Label::createWithSystemFont(
		__String::createWithFormat("当前分数：%d", currentScore)->getCString(), "Arial", 24);
	label->setPosition(Vec2(visibleSize.width / 2,
		origin.y + visibleSize.height / 2 + 60));
	label->setHorizontalAlignment(kCCTextAlignmentRight);
	label->setAnchorPoint(Vec2(0.5, 0.5));
	this->addChild(label, 1);
	//最高分数
	auto label2 = Label::createWithSystemFont(
		__String::createWithFormat("最高分数：%d", topScore)->getCString(), "Arial", 24);
	label2->setPosition(Vec2(visibleSize.width / 2,
		origin.y + visibleSize.height / 2 + 60 - label->getContentSize().height));
	label2->setHorizontalAlignment(kCCTextAlignmentRight);
	label2->setAnchorPoint(Vec2(0.5, 0.5));
	this->addChild(label2, 1);

	//菜单
	auto restartItem = MenuItemImage::create("game_Reagain.png", "game_Reagain.png", 
		CC_CALLBACK_1(GameOverScene::continueGame, this));
	restartItem->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height / 2));

	auto closeItem = MenuItemImage::create("game_over.png", "game_over.png",
		CC_CALLBACK_1(GameOverScene::exitGame, this));
	closeItem->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height / 2 - restartItem->getContentSize().height - 10));

	auto menu = Menu::create(restartItem, closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	return true;
}

//continue game
void GameOverScene::continueGame(cocos2d::Ref* pSender)
{
	auto scene = GameScene::createScene();
	auto gameScene = TransitionSlideInR::create(1.0f, scene);
	Director::getInstance()->replaceScene(gameScene);
}

//exit game
void GameOverScene::exitGame(cocos2d::Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
	return;
#endif

	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}