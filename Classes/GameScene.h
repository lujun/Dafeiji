﻿#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"

class GameScene : public cocos2d::Layer
{
	int status;//游戏状态     1为正常、2为暂停、3为结束
	int score;
	float fx, fy;//记录手指点击的开始位置
	cocos2d::Vector<cocos2d::Sprite *> bulletList;
	cocos2d::Vector<cocos2d::Sprite *> enemyList;
public:
	static cocos2d::Scene * createScene();
	virtual bool init();
	virtual void onEnter();//层进入时调用的方法，碰撞事件监听在此处声明定义
	CREATE_FUNC(GameScene);


	//滚动背景定时器回调函数
	void backgroundMove(float f);

	//菜单回调方法
	void gameClose(cocos2d::Ref* pSender);
	void gamePause(cocos2d::Ref* pSender);
	void gameResume(cocos2d::Ref * pSender);

	//触摸事件，系统监听
	virtual bool onTouchBegan(cocos2d::Touch * touch, cocos2d::Event * event);
	virtual void onTouchMoved(cocos2d::Touch * touch, cocos2d::Event * event);

	//子弹创建的定时器回调函数
	void bulletCreate(float f);
	//让子弹飞和让敌机飞  因为敌机和子弹移动的速度一样，不用创建多个定时器
	void objectMove(float f);
	//创建敌机
	void enemyCreate(float f);
	//飞机爆炸动画
	void planeBomb(cocos2d::Vec2 vec,int tag);
	//飞机爆炸动画播放完之后的回调函数
	void bombRemove(Node * sprite);
	//停止所有的定时器
	void stopAllSchedule();
	//游戏结束
	void gameOver();

};

#endif