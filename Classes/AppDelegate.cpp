﻿#include "AppDelegate.h"
#include "HelloWorldScene.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::create("打飞机");
		//glview->setFrameSize(720, 1280);//设置窗口大小
		director->setOpenGLView(glview);

		//设置分辨率图片适配
		CCSize frameSize = glview->getFrameSize();
		//下面填写背景图片的大小，适配就是放大和缩小，以最大的图片（背景图）为参照，以下大小就是背景图的大小
		CCSize winSize = CCSize(720, 1280);

		//得到一个宽高比，来调整逻辑分辨率的大小
		float widthRate = frameSize.width / winSize.width;
		float heightRate = frameSize.height / winSize.height;

		if (widthRate > heightRate)//逻辑高度偏大，调整逻辑高和逻辑宽一样的比率
		{
			glview->setDesignResolutionSize(winSize.width, 
				winSize.height * heightRate / widthRate, 
				kResolutionNoBorder);
		}
		else//否则调整逻辑宽的比率
		{
			glview->setDesignResolutionSize(winSize.width * widthRate / heightRate, 
				winSize.height, 
				kResolutionNoBorder);
		}

    }

    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    auto scene = HelloWorld::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
