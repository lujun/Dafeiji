﻿#include "GameScene.h"
#include "GameOverScene.h"

USING_NS_CC;
using namespace CocosDenshion;

Scene* GameScene::createScene()
{
	auto scene = Scene::createWithPhysics();//设置物理世界场景
	//测试用
	//PhysicsWorld* phyWorld = scene->getPhysicsWorld();//获取物理世界对象
	//phyWorld->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);//设置为物理世界物体课件
	//////////////////////////////////////////////////////////////////////////
	auto layer = GameScene::create();
	scene->addChild(layer);
	return scene;
}

bool GameScene::init()
{
	if (!Layer::init())
	{
		return false;
	}
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	status = 1;
	score = 0;

	//分数label
	auto label = Label::createWithSystemFont("分数：0", "Aral", 24);
	label->setTag(100);
	label->setPosition(Vec2(0,
		origin.y + visibleSize.height - label->getContentSize().height / 2));
	label->setHorizontalAlignment(kCCTextAlignmentRight);
	label->setAnchorPoint(Vec2(0, 0.5));
	this->addChild(label, 1);

	//创建背景音乐
	SimpleAudioEngine::getInstance()->playBackgroundMusic("sound/game_music.mp3");

	//创建滚动背景
	auto bg1 = Sprite::create("ui/shoot_background/background.png");
	bg1->setPosition(Vec2(origin.x + visibleSize.width / 2, 0));
	bg1->setAnchorPoint(Vec2(0.5, 0)); 
	bg1->setTag(101);
	this->addChild(bg1, 0);

	auto bg2 = Sprite::create("ui/shoot_background/background.png");
	bg2->setPosition(Vec2(origin.x + visibleSize.width / 2, 
		bg1->getPositionY() + bg1->getContentSize().height));
	bg2->setAnchorPoint(Vec2(0.5, 0));
	bg2->setTag(102);
	this->addChild(bg2, 0);

	//滚动
	this->schedule(schedule_selector(GameScene::backgroundMove), 0.01);

	//创建自己的飞机
	auto plane = Sprite::create("ui/shoot/hero1.png");
	plane->setPosition(Vec2(visibleSize.width / 2 + origin.x, 200));
	plane->setTag(103);
	//设置物体形状，碰撞属性
	//碰撞掩码，用于判断是否可以与其他物体碰撞，
	//判断逻辑为两者的碰撞掩码进行逻辑与，如为零则不会碰撞，否则会
	//这里只有我机、敌机、子弹，子弹和敌机可以碰撞，敌机和我机可以碰撞
	//我机和子弹不可以碰撞，以次可以设置它们的掩码分别为0x01(0001)、0x03(0011)、0x02(0010)
	auto planeBody = PhysicsBody::createBox(plane->getContentSize());//设置的刚体为矩形
																	 //也可以为其他形状
	planeBody->setContactTestBitmask(0x0003);//碰撞测试掩码
	planeBody->setCategoryBitmask(0x0001);//类别掩码
	planeBody->setCollisionBitmask(0x0007);//碰撞掩码
	planeBody->setGravityEnable(false);//设置重力无效
	plane->setPhysicsBody(planeBody);//把物体设置为实体

	this->addChild(plane);

	//飞机帧动画
	Animation* animation = Animation::create();
	SpriteFrame* spriteFrame1 = SpriteFrame::create("ui/shoot/hero1.png", 
		Rect(0, 0, 102, 126));
	SpriteFrame* spriteFrame2 = SpriteFrame::create("ui/shoot/hero2.png", 
		Rect(0, 0, 102, 126));
	animation->addSpriteFrame(spriteFrame1);
	animation->addSpriteFrame(spriteFrame2);
	animation->setDelayPerUnit(0.15f);//两张图片交互间隔
	Animate* animate = Animate::create(animation);
	plane->runAction(RepeatForever::create(animate));

	//触摸事件注册，要通过回调函数来控制飞机的坐标
	setTouchEnabled(true);
	//单点触摸
	setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
	//我机发射子弹
	this->schedule(schedule_selector(GameScene::bulletCreate), 0.3);
	//子弹和敌机飞
	this->schedule(schedule_selector(GameScene::objectMove), 0.01);
	//敌机创建
	this->schedule(schedule_selector(GameScene::enemyCreate), 0.5);

	return true;
}

void GameScene::backgroundMove(float f)
{
	auto bg1 = this->getChildByTag(101);
	auto bg2 = this->getChildByTag(102);

	if (bg2->getPositionY() + bg2->getContentSize().height 
		<= Director::getInstance()->getVisibleSize().height)
	{
		bg1->setPositionY(-bg1->getContentSize().height 
			+ Director::getInstance()->getVisibleSize().height);
	}
	bg1->setPositionY(bg1->getPositionY() - 3);
	bg2->setPositionY(bg1->getPositionY()+ bg1->getContentSize().height);
}

//层进入后会调用该函数，进行物理世界的碰撞检测
void GameScene::onEnter()
{
	Layer::onEnter();

	auto listener = EventListenerPhysicsContact::create();
	listener->onContactBegin = [=] (PhysicsContact& contact)
	{
		auto spriteA = (Sprite*) contact.getShapeA()->getBody()->getNode();
		auto spriteB = (Sprite*) contact.getShapeB()->getBody()->getNode();

		int tag1 = spriteA->getTag();
		int tag2 = spriteB->getTag();

		Vec2 vec1 = spriteA->getPosition();
		Vec2 vec2 = spriteB->getPosition();

		//sprite tag
		//103 plane
		//104 enemy1
		//105 enemy2
		//106 bullet
		if (tag1 + tag2 == 210 || tag1 + tag2 == 211)//子弹和敌机碰撞
		{
			SimpleAudioEngine::getInstance()->playEffect("sound/use_bomb.mp3");
			if (tag1 == 104 || tag2 == 104)//enemy1
			{
				score += 500;
			}
			else//enemy2
			{
				score += 1000;
			}
			auto scoreSprite = (Label*)this->getChildByTag(100);
			scoreSprite->setString(String::createWithFormat("分数：%d", score)->_string);

			//启动粒子效果
			//auto system = ParticleSystemQuad::create("");
			auto system = ParticleExplosion::create();

			if (tag1 == 104 || tag1 == 105)//spriteA是子弹
			{
				enemyList.eraseObject(spriteA);
				bulletList.eraseObject(spriteB);
				system->setPosition(vec1);
				//启动动画
				this->planeBomb(vec1, tag1);
			}
			else//spriteB是子弹
			{
				enemyList.eraseObject(spriteB);
				bulletList.eraseObject(spriteA);
				system->setPosition(vec2);
				//启动动画
				this->planeBomb(vec2, tag2);
			}
			//粒子特效加入层中
			//this->addChild(system);
			spriteA->removeFromParent();
			spriteB->removeFromParent();
		}
		else if (tag1 + tag2 == 207 || tag1 + tag2 == 208)//我机与敌机碰撞
		{
			SimpleAudioEngine::getInstance()->playEffect("sound/game_over.mp3");
			//修改游戏状态3
			status = 3;
			stopAllSchedule();//停止所有定时器
			if (tag1 == 103)
			{
				planeBomb(vec2, tag2);
				planeBomb(vec1, tag1);
			}
			else
			{
				planeBomb(vec1, tag1);
				planeBomb(vec2, tag2);
			}

			//使用UserDefault存储些用户数据分数
			UserDefault* userDefault = UserDefault::getInstance();
			int topScore = userDefault->getIntegerForKey("topscore");
			if (topScore < score)
			{
				userDefault->setIntegerForKey("topscore", score);
			}
			else
			{
				userDefault->setIntegerForKey("topscore", topScore);	
			}
			userDefault->setIntegerForKey("currentscore", score);

			spriteA->removeFromParent();
			spriteB->removeFromParent();
		}
		else//敌机与敌机碰撞
		{

		}
		return true;
	};

	//注册监听器
	EventDispatcher* eventDispatcher = Director::getInstance()->getEventDispatcher();
	eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

bool GameScene::onTouchBegan(cocos2d::Touch * touch, cocos2d::Event * event)
{
	if (status == 1)
	{
		fx = touch->getLocation().x;
		fy = touch->getLocation().y;
	}
	return true;
}

void GameScene::onTouchMoved(cocos2d::Touch * touch, cocos2d::Event * event)
{
	if (status == 1)
	{
		int mx = touch->getLocation().x - fx;
		int my = touch->getLocation().y - fy;
		auto spPlane = this->getChildByTag(103);
		spPlane->runAction(MoveBy::create(0, Point(mx, my)));
		fx = touch->getLocation().x;
		fy = touch->getLocation().y;
	}
}

//创建子弹
void GameScene::bulletCreate(float f)
{
	SimpleAudioEngine::getInstance()->playEffect("sound/bullet.mp3");
	auto plane = this->getChildByTag(103);
	Sprite* bullet = Sprite::create("ui/shoot/bullet2.png");
	bullet->setPosition(Vec2(plane->getPosition().x, plane->getPosition().y + 60));
	bullet->setTag(106);

	auto bulletBody = PhysicsBody::createBox(bullet->getContentSize());
	bulletBody->setContactTestBitmask(0x002);
	bulletBody->setCategoryBitmask(0x005);
	bulletBody->setCollisionBitmask(0x002);
	bulletBody->setGravityEnable(false);
	bullet->setPhysicsBody(bulletBody);//把物体添加到精灵中

	this->addChild(bullet);
	this->bulletList.pushBack(bullet);
}

//我机和子弹移动
void GameScene::objectMove(float f)
{
	//遍历所有的子弹，让子弹上移
	for (int i = 0; i < bulletList.size(); i++)
	{
		auto bullet = bulletList.at(i);
		bullet->setPositionY(bullet->getPositionY() + 3);
		//超过屏幕的范围，移除
		if (bullet->getPositionY() > Director::getInstance()->getWinSize().height)
		{
			bullet->removeFromParent();//从层中移除
			bulletList.eraseObject(bullet);//将bullet从Vector中移除
			i--;
		}
	}
	//遍历所有敌机，让敌机下移
	for (int i = 0; i < enemyList.size(); i++)
	{
		auto enemy = enemyList.at(i);
		enemy->setPositionY(enemy->getPositionY() - 5);
		//超过屏幕的范围，移除
		if (enemy->getPositionY() < -enemy->getContentSize().height)
		{
			enemy->removeFromParent();
			enemyList.eraseObject(enemy);
			i--;
		}
	}
}

//敌机创建
void GameScene::enemyCreate(float f)
{
	int random = rand() % 2 + 1;
	auto string = cocos2d::__String::createWithFormat("ui/shoot/enemy%d.png", random);
	auto enemy = Sprite::create(string->getCString());
	if (random == 1)
	{
		enemy->setTag(104);
	}
	else
	{
		enemy->setTag(105);
	}
	enemy->setPosition(Vec2(rand()%(int)(Director::getInstance()->getVisibleSize().width),
		Director::getInstance()->getVisibleSize().height + enemy->getContentSize().height));
	auto enemyBody = PhysicsBody::createBox(enemy->getContentSize());
	enemyBody->setContactTestBitmask(0x0003);
	enemyBody->setCategoryBitmask(0x0002);
	enemyBody->setCollisionBitmask(0x0001);
	enemyBody->setGravityEnable(false);
	enemy->setPhysicsBody(enemyBody);
	this->addChild(enemy);
	this->enemyList.pushBack(enemy);
}

//飞机爆炸动画
void GameScene::planeBomb(cocos2d::Vec2 vec,int tag)
{
	float timeDelay = 0.1;
	Vector<SpriteFrame*> aniamtionFrame;
	if (tag == 104)//小敌机爆炸
	{
		for (int i = 0; i < 4; i++)
		{
			auto string = cocos2d::__String::createWithFormat
				("ui/shoot/enemy1_down%d.png", i + 1);
			SpriteFrame* sf = SpriteFrame::create
				(string->getCString(), Rect(0, 0, 57, 51));
			aniamtionFrame.pushBack(sf);
		}
	}
	else if (tag == 105)//大敌机爆炸动画
	{
		for (int i = 0; i < 4; i++)
		{
			auto string = cocos2d::__String::createWithFormat
				("ui/shoot/enemy2_down%d.png", i + 1);
			SpriteFrame* sf = SpriteFrame::create
				(string->getCString(), Rect(0, 0, 69, 95));
			aniamtionFrame.pushBack(sf);
		}
	}
	else//我机爆炸动画
	{
		timeDelay = 0.5;
		for (int i = 0; i < 4; i++)
		{
			auto string = cocos2d::__String::createWithFormat
				("ui/shoot/hero_blowup_n%d.png", i + 1);
			SpriteFrame* sf = SpriteFrame::create
				(string->getCString(), Rect(0, 0, 102, 126));
			aniamtionFrame.pushBack(sf);
		}
	}
	Animation* ani = Animation::createWithSpriteFrames(aniamtionFrame, timeDelay);
	auto blankSprite = Sprite::create();
	blankSprite->setTag(tag);

	Action* act = Sequence::create(Animate::create(ani), 
		CCCallFuncN::create(blankSprite, callfuncN_selector(GameScene::bombRemove)), NULL);
	this->addChild(blankSprite);
	blankSprite->setPosition(vec);
	blankSprite->runAction(act);
}

//移除爆炸动画
void GameScene::bombRemove(Node * sprite)
{
	sprite->removeFromParentAndCleanup(true);
	if (sprite->getTag() == 103)
	{
		SimpleAudioEngine::getInstance()->stopBackgroundMusic("sound/game_music.mp3");
		gameOver();
	}
}

//停止所有定时器
void GameScene::stopAllSchedule()
{
	this->unscheduleAllSelectors();
}

//游戏结束
void GameScene::gameOver()
{
	auto scene = GameOverScene::createScene();
	auto gameOverScene = TransitionTurnOffTiles::create(1.0f, scene);
	Director::getInstance()->replaceScene(gameOverScene);
}

