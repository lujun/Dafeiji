﻿#include "HelloWorldScene.h"
#include "GameScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();


	auto bg1 = Sprite::create("ui/shoot_background/background.png");

	//注意，Vec2是Cocos2d-x的数据结构，里面包含两个float型的数据，分别用来表示x轴和y轴。 
	//setPosition是设置精灵的相对屏幕的位置（也就是世界坐标）。
	//当前这两个参数表示设置精灵在屏幕的宽度一半，高度为零的位置，即屏幕最下方的中间位置。
	//Cocos2d-x是以OpenGl的坐标的右手坐标为标准，屏幕的左下方为原点。
	//visibleSize.width和height是屏幕的宽高， 
	bg1->setPosition(Vec2(origin.x + visibleSize.width / 2, 0));

	//注意这里的锚点设置，为了图片永远在屏幕中间，x轴必须设置为0.5，
	//其中的意思就是这个精灵向左移动精灵的一半宽度，y轴则是向下 
	bg1->setAnchorPoint(Vec2(0.5, 0));

	//设置Tag，以后可以通过这个标签找到这个精灵，以便对这个精灵进行操作。 
    bg1->setTag(101);

	////将背景精灵添加到层中。可以认为是当前的屏幕。
	//其中的0是设置可见优先权，数值越小优先权越小，即当如果有其他的精灵的优先权比他大时，他会被遮挡住的
	this->addChild(bg1, 0);

	//第二张背景图，是跟在第一张的上面，无缝连接，两张图形成不间断的地图滚动 
	//getContentSize是获取精灵的size其中有它的宽度和高度。
	auto bg2 = Sprite::create("ui/shoot_background/background.png");
	bg2->setPosition(Vec2(origin.x + visibleSize.width / 2, 
		bg1->getPositionY() + bg1->getContentSize().height));
    bg2->setAnchorPoint(Vec2(0.5, 0));
	bg2->setTag(102);
	this->addChild(bg2, 0);

	//创建菜单
	auto startItem = MenuItemImage::create("game_start.png", "game_start.png", 
		CC_CALLBACK_1(HelloWorld::menuStartCallback,this));
	startItem->setPosition(Vec2(visibleSize.width / 2 + origin.x, 
		visibleSize.height / 2 + origin.y));

	auto closeItm = MenuItemImage::create("game_over.png", "game_over.png",
		CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
	closeItm->setPosition(Vec2(visibleSize.width / 2 + origin.x, 
		visibleSize.height /2 + origin.y - startItem->getContentSize().height - 10));

	auto menu = Menu::create(startItem, closeItm, NULL);
	menu->setPosition(Vec2::ZERO);

	this->addChild(menu, 1);

	//创建首页的飞机
	auto plane = Sprite::create("ui/shoot/hero1.png");
	plane->setPosition(Vec2(visibleSize.width / 2 + origin.x, 200));
	this->addChild(plane);

	//飞机帧动画
	Animation* animation = Animation::create();
	SpriteFrame* spriteFrame1 = SpriteFrame::create("ui/shoot/hero1.png", 
		Rect(0, 0, 102, 126));
	SpriteFrame* spriteFrame2 = SpriteFrame::create("ui/shoot/hero2.png", 
		Rect(0, 0, 102, 126));
	animation->addSpriteFrame(spriteFrame1);
	animation->addSpriteFrame(spriteFrame2);
	animation->setDelayPerUnit(0.15f);//两张图片交互间隔
	Animate* animate = Animate::create(animation);
	plane->runAction(RepeatForever::create(animate));

	//plane->stopAllActions(); //停止所有动画，应该是清除了动画的内存。

	//
	this->schedule(schedule_selector(HelloWorld::backgroundMove), 0.01);

    return true;
}

void HelloWorld::backgroundMove(float f){
	auto bg1 = this->getChildByTag(101);
	auto bg2 = this->getChildByTag(102);

	//第二张背景图片滚动到临界点时切换两张背景图片的坐标
	if (bg2->getPositionY() + bg2->getContentSize().height 
		<= Director::getInstance()->getVisibleSize().height)
	{
		bg1->setPositionY(-bg1->getContentSize().height 
			+ Director::getInstance()->getVisibleSize().height);
	}
	bg1->setPositionY(bg1->getPositionY() - 3);
	bg2->setPositionY(bg1->getPositionY()+ bg1->getContentSize().height);
}

void HelloWorld::menuStartCallback(Ref* pSender)
{
	auto scene = GameScene::createScene();
	auto gameScene = TransitionSlideInR::create(1.0f, scene);
	Director::getInstance()->replaceScene(gameScene);
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
