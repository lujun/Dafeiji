﻿#ifndef __GAMEOVER_SCENE_H__
#define __GAMEOVER_SCENE_H__

#include "cocos2d.h"

class GameOverScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene * createScene();
	virtual bool init();
	CREATE_FUNC(GameOverScene);

	void continueGame(cocos2d::Ref* pSender);
	void exitGame(cocos2d::Ref* pSender);
};

#endif